# SEU Physics Experiment
There are sources (especially codes) related to Physics experiments of Southeast University.

## Digital Storage Oscilloscope
### Measurement of Diode Forward Volt-Ampere Characteristic
This project is about the fitting based on data from the experiment.
> Note: The final version of the figure is edited based on the code.

## Photoelectric Effect
Use data collected in the experiment that is saved in Excel.
### Turn On Voltage
The method of fitting is `poly1`.
> Note: The figure created needs to be saved.
### Volt Ampere Characteristic
There are three graphs under different experiment circumstances.
The method of fitting is `smoothingspline`.
> Note: The figures are automatically saved to a PDF files.
